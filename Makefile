PREFIX = /usr
FONT_DIR = "$(PREFIX)/share/fonts/speedwm-fonts"

install:
	mkdir -p "$(FONT_DIR)"
	cp -rf 'speedwm-font.ttf' "$(FONT_DIR)"
	fc-cache -fv

uninstall:
	rm -rf "$(FONT_DIR)/speedwm-font.ttf" "$(FONT_DIR)/speedwm-font.ttf"

