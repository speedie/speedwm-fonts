# speedwm-fonts

Fonts for speedwm

# Credit

This is a fork of [this repository](https://github.com/My-DWM-Environment/fonts).
It also includes [nerd fonts](https://www.nerdfonts.com).
